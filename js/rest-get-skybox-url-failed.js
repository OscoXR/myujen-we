import {Component, Property} from '@wonderlandengine/api';

/**
 * rest-get-skybox-url
 */
export class RestGetSkyboxUrl extends Component {
  static TypeName = 'rest-get-skybox-url';

  /* Properties that are configurable in the editor */
  static Properties = {
    skyboxObject: Property.object(),
  };

  /* Add other component types here that your component may
    * create. They will be registered with this component */
  static Dependencies = [];

  init() {
    // console.log('init() with param', this.param);
  }

  start() {
    // -- Set Skybox Object Params (TEST ONLY)
    // console.log("skybox import???")
    // console.log(this.skyboxObject.getComponent("image-texture"))
    // this.url = this.skyboxObject.getComponent("image-texture").url
    // console.log("init skybox url :" + this.url)

    // -- Get Params from URL
    const pop = window.location.href.split('/').pop(); // -- get last part of url
    let id = pop.split('=')[1];

    // -- Set up request to retrieve world info from API endpoint
    var apiUrl = "https://myujen-nextjs.vercel.app/api/world/" + id;
    let skyboxUrl;
    fetch(apiUrl)
      .then(response => response.json())
      .then(data => {
        // -- Do something with the data returned from the API
        skyboxUrl = data.ImageUrl
        console.log("skybox url: " + skyboxUrl)
        skyboxUrl = "https://myujen.s3.us-east-2.amazonaws.com/skyboxes/test.jpg" // TODO: DELETE LATER
        // this.url = skyboxUrl
        // console.log("NEW skybox url: " + this.url)
        // this.skyboxObject.getComponent("image-texture").url = skyboxUrl // TODO: FIX LATER
      });
  }

  update(dt) {
    /*  Called every frame. */
  }
}
