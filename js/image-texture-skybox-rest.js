import {Component, Property} from '@wonderlandengine/api';
import {setFirstMaterialTexture} from './utils.js';

/**
 * Downloads an image from URL and applies it as `diffuseTexture` or `flatTexture`
 * to an attached mesh component.
 *
 * Materials from the following shaders are supported:
 *  - "Phong Opaque Textured"
 *  - "Flat Opaque Textured"
 *  - "Background"
 *  - "Physical Opaque Textured"
 *  - "Foliage"
 */
export class ImageTexture extends Component {
    static TypeName = 'image-texture-skybox-rest';
    static Properties = {
        /** URL to download the image from */
        url: Property.string(),
        /** Material to apply the video texture to */
        material: Property.material(),
        /** Name of the texture property to set */
        textureProperty: Property.string('auto'),
    };

    start() {
        if (!this.material) {
            throw Error('image-texture: material property not set');
        }

        // -- Get Params from URL
        const pop = window.location.href.split('/').pop(); // -- get last part of url
        let id = pop.split('=')[1];
        this.url = `https://myujen.s3.us-east-2.amazonaws.com/skyboxes/${id}.jpg`
        console.log("skybox url: " + this.url)
        this.engine.textures
        .load(this.url, 'anonymous')
        .then((texture) => {
            const mat = this.material;
            if (!setFirstMaterialTexture(mat, texture, this.textureProperty)) {
                console.error('Shader', mat.shader, 'not supported by image-texture');
            }
        })
        .catch(console.err);

        // REST Call Not Needed KEKW
        // var apiUrl = "https://myujen-nextjs.vercel.app/api/world/" + id;
        // let skyboxUrl;
        // fetch(apiUrl)
        // .then(response => response.json())
        // .then(data => {
        //     // -- Do something with the data returned from the API
        //     skyboxUrl = data.ImageUrl
        //     console.log("skybox url: " + skyboxUrl)
        //     this.url = skyboxUrl
        //     this.engine.textures
        //     .load(this.url, 'anonymous')
        //     .then((texture) => {
        //         const mat = this.material;
        //         if (!setFirstMaterialTexture(mat, texture, this.textureProperty)) {
        //             console.error('Shader', mat.shader, 'not supported by image-texture');
        //         }
        //     })
        //     .catch(console.err);
        // });
    }
}